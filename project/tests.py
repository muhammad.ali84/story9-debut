from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from .views import *

import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class ModelTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(
            username = 'waduh',
            email = "waduhlogi@gmail.com",
            password = 'kurakuraninja',
        )

    def test_instance_created(self):
        self.assertEqual(User.objects.count(), 1)

class UrlsTest(TestCase):

    def setUp(self):
        self.index = reverse("project:index")
        self.login = reverse("project:loginPage")
        self.register = reverse("project:registerPage")
        self.logout = reverse("project:logoutPage")
    
    def test_index_use_right_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)

    def test_login_use_right_function(self):
        found = resolve(self.login)
        self.assertEqual(found.func, loginPage)

    def test_register_use_right_function(self):
        found = resolve(self.register)
        self.assertEqual(found.func, registerPage)

    def test_logout_use_right_function(self):
        found = resolve(self.logout)
        self.assertEqual(found.func, logoutPage)

class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.index = reverse("project:index")
        self.login = reverse("project:loginPage")
        self.register = reverse("project:registerPage")
        self.logout = reverse("project:logoutPage")

    def test_GET_index(self):
        # If no one login it redirect to login page
        response1 = self.client.get(self.index, follow=True)
        self.assertEqual(response1.status_code, 200)
        self.assertIn("Welcome", str(response1.content))
        # If a user login it show her name
        user = User.objects.create(username='jane_doe')
        user.set_password('12345')
        user.save()
        self.client.login(username='jane_doe', password='12345')        
        response2 = self.client.get(self.index)
        self.assertEqual(response2.status_code, 200)
        self.assertTemplateUsed(response2, "project/index.html")
        self.assertIn("jane_doe", str(response2.content))

    def test_GET_register(self):
        response = self.client.get(self.register)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "project/register.html")
        self.assertIn("register", str(response.content))

    def test_POST_register_invalid(self):
        response = self.client.post(self.register, {
            'username': 'aliasadillah',
            'password1': 'bla',
            'password2': 'bla'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "project/register.html")
        self.assertIn("This password is too short", str(response.content))

    def test_POST_register_valid(self):
        response = self.client.post(self.register, {
            'username': 'aliasadillah',
            'password1': 'asdfghjkl69',
            'password2': 'asdfghjkl69'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "project/login.html")
        self.assertIn("Your account created successfully, please login", str(response.content))

    def test_GET_login(self):
        response = self.client.get(self.login)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "project/login.html")
        self.assertIn("Welcome Back!", str(response.content))

    def test_POST_login_invalid(self):
        response = self.client.post(self.login, {
            'username': 'aliasadillah',
            'password': 'passwordsalah',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "project/login.html")
        self.assertIn("Username or password is incorrect", str(response.content))

    def test_POST_login_valid(self):
        user = User.objects.create(username='aliasadillah')
        user.set_password('asdfghjkl69')
        user.save()
        response = self.client.post(self.login, {
            'username': 'aliasadillah',
            'password': 'asdfghjkl69',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "project/index.html")
        self.assertIn("aliasadillah", str(response.content))

    def test_GET_logout(self):
        # If a user login, this view should logout the user and redirect to loginpage
        user = User.objects.create(username='jane_doe')
        user.set_password('12345')
        user.save()
        self.client.login(username='jane_doe', password='12345')      
        response = self.client.get(self.logout, follow=True) 
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "project/login.html")
        self.assertIn("Logout successfully", str(response.content))
