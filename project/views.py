from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required(login_url='project:loginPage')
def index(request):
    user = request.user
    context = {'user': user}
    return render(request, 'project/index.html', context)

def registerPage(request):
    form = UserCreationForm()
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get("username")
            messages.success(request, "Your account created successfully, please login")
            return redirect('project:loginPage')
    context = {'form':form}
    return render(request, 'project/register.html', context)

def loginPage(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('project:index')
        else:
            messages.info(request, 'Username or password is incorrect')
    context = {}
    return render(request, 'project/login.html', context)

def logoutPage(request):
    logout(request)
    messages.success(request, "Logout successfully")
    return redirect('project:loginPage')
